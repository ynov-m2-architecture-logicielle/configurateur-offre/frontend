import { Image } from "./image";
import { Tag } from "./tag";

export class Product {
    
	id:string ='';
	name :string= '';
	price :Number = 0;
	description : string ='';
	images : Image [] = [] ;
    //tag:Tag = new Tag();
}
