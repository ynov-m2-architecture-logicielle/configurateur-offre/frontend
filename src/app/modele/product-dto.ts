export class ProductDto {
    id:string ='';
	name :string= '';
	price :Number = 0;
	description : string ='';
	images : string [] = [] ;
}
