import { Product } from "./product";

export class Tag {
    public id : string ='';
    public name: string ='';
    public category:string ='';
    public product:Product = new Product(); 
}
