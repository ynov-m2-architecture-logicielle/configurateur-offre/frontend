import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from '../modele/product';
import { ProductDto } from '../modele/product-dto';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
   private productUrl = `${environment.apiBase}`
  constructor(private http: HttpClient) { }

    getAllProduct(): Observable<any> {
     return this.http.get<any>(`${this.productUrl}/products`);
    }

    upload(file: File): Observable<HttpEvent<any>> {
      const formData: FormData = new FormData();
    
      formData.append('file', file);
    
      const req = new HttpRequest('POST', `${this.productUrl}/upload`, formData, {
        reportProgress: true,
        responseType: 'json'
      });
    
      return this.http.request(req);
    }
   save(product:ProductDto) : Observable<any>{
     return this.http.post<any>(`${this.productUrl}/save`, product);
   }
   update(product:ProductDto, id:string){
    return this.http.put<any>(`${this.productUrl}/update?id=${id}`, product);
  }
  delete(product:Product, id:string){
    return this.http.put<any>(`${this.productUrl}/delete?id=${id}`, product);
  }
}
