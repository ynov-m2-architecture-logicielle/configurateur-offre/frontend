import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';

import { Product } from '../modele/product';
import { ProductService } from '../services/product.service';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ProductDto } from '../modele/product-dto';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  isShow: boolean = false;
  isShowUpdate : boolean = false;
  selectedFile: File | undefined;

  

  products:Product[] = [];
  displayedColumns: string[] = ['name', 'price', 'description'];
  addedForm: FormGroup = new FormGroup({});
  updatedForm:FormGroup = new FormGroup({});
  productUpdated : Product = new Product();
  productedUpdatedDto : ProductDto = new ProductDto();
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort = new MatSort();
  public dataSource: MatTableDataSource<Product> = new MatTableDataSource();
  constructor(private productService:ProductService, private dialog: MatDialog, private formBuilder: FormBuilder,private cd: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.getAllProduct();
    this.initForm();
    this.init();
  }
  
  initForm(){
    this.addedForm = this.formBuilder.group({
      name:new FormControl('',Validators.required),
      price:new FormControl('',Validators.required),
      description: new FormControl('',Validators.required)
    });
  }

  productDto : ProductDto = new ProductDto();

  get name(){
    return this.addedForm.get('name') as FormControl;
  }
  get price(){
    return this.addedForm.get('price') as FormControl;
  }
  get description(){
    return this.addedForm.get('description') as FormControl;
  }
  
  init(){
    this.updatedForm = this.formBuilder.group({
      nameU:new FormControl('',Validators.required),
      priceU:new FormControl('',Validators.required),
      descriptionU: new FormControl('',Validators.required)
    });
  }

  get nameU(){
    return this.updatedForm.get('nameU') as FormControl;
  }
  get priceU(){
    return this.updatedForm.get('priceU') as FormControl;
  }
  get descriptionU(){
    return this.updatedForm.get('descriptionU') as FormControl;
  }
  
  add(){
      if(this.addedForm.invalid){
        return ;
      }
      this.productDto = new ProductDto();
      this.productDto.price = this.price.value;
      this.productDto.name = this.name.value;
      this.productDto.images = [this.name.value];
      this.productDto.description = this.description.value;
      this.productService.save(this.productDto).subscribe(
        res=>{
          this.productDto = res;
          console.log(res);
          this.getAllProduct();
          this.isShow = false;
        },error=>{
          console.log(error);
        }
      )
      
  }
  public onFileChanged(event:any) {

        this.selectedFile = event.target.files[0];
      }
    ishowFormUpdate(product:Product){
      this.isShowUpdate = !this.isShowUpdate;
      console.log(product);
      this.productUpdated = product;
    }
    
  showForm(){
    this.isShow = !this.isShow;
  }
  /**
   * 
   */
  getAllProduct(){
    this.productService.getAllProduct().subscribe(
     data=>{
      this.products = data;
      console.log(data);
        this.dataSource = new MatTableDataSource<Product>(this.products);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
     },error=>{
       console.log(error);
     }
    )
  }
  reset(){
    this.addedForm.reset();
    this.initForm();
  }
  resetU(){
    this.updatedForm.reset();
    this.init();
    this.getAllProduct();
  }
  /**
   * 
   * @returns 
   */
  update(){
    if(this.updatedForm.invalid){
      return ;
    }
    this.productedUpdatedDto.description = this.productUpdated.description;
    this.productedUpdatedDto.name = this.productUpdated.name;
    this.productedUpdatedDto.price = this.productUpdated.price;
    this.productService.update(this.productedUpdatedDto, this.productUpdated.id).subscribe(
      data=>{
        this.productUpdated = data;
        console.log(data);
        this.getAllProduct();
        this.isShowUpdate = false;
       },error=>{
         console.log(error);
       }
    );
  }
/**
 * 
 * @param product 
 */
delete(product:Product){
  this.productService.delete(product, product.id).subscribe(
    data=>{
      this.productUpdated = data;
      console.log(data);
      this.getAllProduct();
     },error=>{
       console.log(error);
     }
  )}
}

export class DialogContentExampleDialog {}